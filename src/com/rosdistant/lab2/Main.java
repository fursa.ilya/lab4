package com.rosdistant.lab2;

/**
 * Есть животные, которые издают звуки. Есть животные имеющие крылья. И,
 * наконец, есть животные, имеющие шерсть. Разработать UML модель и закодировать
 * структуру классов и интерфейсов.
 */
public class Main {

    public static void main(String[] args) {
        Calibre calibre = new Calibre("Woody"); //Создаем калибри
        Wolf wolf = new Wolf("Sherman"); //Создаем волка

        System.out.println("Калибри имеет крылья: " + calibre.hasWings());
        System.out.println("Калибри умеет громко выть: " + calibre.hasVoice());
        System.out.println("Калибри имеет шерсть: " + calibre.hasWool());

        System.out.println("Волк имеет крылья: " + wolf.hasWings());
        System.out.println("Волк умеет громко выть: " + wolf.hasVoice());
        System.out.println("Волк имеет шерсть: " + wolf.hasWool());
    }
}
