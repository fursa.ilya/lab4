package com.rosdistant.lab2;

public interface Animalable {

    boolean hasVoice();

    boolean hasWings();

    boolean hasWool();

}
