package com.rosdistant.lab2;

//Животеное издающее звуки
public class Wolf extends Animal implements Animalable {
    private String name;

    public Wolf(String name) {
        super(name);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean hasVoice() { //Волк может подавать голос
        return true;
    }

    @Override
    public boolean hasWings() { //У волка нет крыльев
        return false;
    }

    @Override
    public boolean hasWool() { //У волка есть шерсть
        return true;
    }
}
