package com.rosdistant.lab2;

public abstract class Animal {
    protected String name;

    public Animal(String name) {
        this.name = name;
    }

    protected void printName() {
        System.out.println(name);
    }
}
