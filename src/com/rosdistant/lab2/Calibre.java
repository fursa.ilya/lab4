package com.rosdistant.lab2;

//Птица калибри
public class Calibre extends Animal implements Animalable {
    private String name;

    public Calibre(String name) {
        super(name);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean hasVoice() { //У калибри нет громкого голоса как у волка к примеру
        return false;
    }

    @Override
    public boolean hasWings() { //Но калибри имеет крылья
        return true;
    }

    @Override
    public boolean hasWool() { //А также у нее нет шерсти
        return false;
    }
}
