package com.rosdistant.lab3;

//Создаем интерфейс для игры, в котором будут
// методы броска и нахождения победной комбинации
public interface Playable {

    int doStep();

    boolean isWon(int step);
}
