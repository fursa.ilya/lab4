package com.rosdistant.lab3;

import java.util.Random;

public class Game implements Playable {

    @Override
    public int doStep() {
        return (int)(Math.random() * 6) + 1;
    }

    @Override
    public boolean isWon(int step) {
        if(step == 6) {
            return true;
        }
        return false;
    }
}
