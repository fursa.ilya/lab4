package com.rosdistant.lab3;

//Игра в кости с использованем интерфейсов
public class Main {

    public static void main(String[] args) {
        Game newGame = new Game();
        for (int i = 1; i < 10; i++) {
            int step = newGame.doStep();
            boolean result = newGame.isWon(step);
            if(result == false) {
                System.out.println("Бросок " + i + " закончился неудачей");
                continue;
            } else {
                System.out.println("Вы одержали победу за " + i + " бросков");
                break;
            }
        }
    }
}
